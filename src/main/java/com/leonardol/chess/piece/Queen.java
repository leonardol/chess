package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.springframework.stereotype.Component;

@Component
public class Queen extends GenericPiece {

    @Override
    public boolean canMove(Position from, Position to, Board board) {
        return !board.isOutOfRangeValue(from, to) && isCorrectMove(from, to) && isFreePath(from, to, board);
    }

    private boolean isCorrectMove(Position from, Position to) {
        return isMovingDiagonal(from, to) || isMovingNonDiagonal(from, to);
    }

    public boolean isFreePath(Position from, Position to, Board board) {
        return isMovingDiagonal(from, to) && isFreeDiagonalPath(from, to, board)
                || isMovingNonDiagonal(from, to) && isFreeNonDiagonalPath(from, to, board);
    }
}
