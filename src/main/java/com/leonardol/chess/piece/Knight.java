package com.leonardol.chess.piece;

import com.leonardol.chess.bean.DeltaMove;
import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.springframework.stereotype.Component;

@Component
public class Knight extends GenericPiece {


    @Override
    public boolean canMove(Position from, Position to, Board board) {
        return !board.isOutOfRangeValue(from, to) && isCorrectMove(from, to);
    }

    public boolean isCorrectMove(Position from, Position to) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        return Math.abs(deltaMove.getFile()) == 2 && Math.abs(deltaMove.getRank()) == 1
                || Math.abs(deltaMove.getRank()) == 2 && Math.abs(deltaMove.getFile()) == 1;
    }

}
