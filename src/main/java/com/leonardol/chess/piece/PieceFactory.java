package com.leonardol.chess.piece;

import com.leonardol.chess.board.Board;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PieceFactory {

    Rook rook;
    Knight knight;
    Bishop bishop;
    Queen queen;
    King king;
    Pawn pawn;
    Board board;

    private Piece getBlackPiece(char piece) {
        return switch (piece) {
            case 'r' -> rook;
            case 'n' -> knight;
            case 'b' -> bishop;
            case 'q' -> queen;
            case 'k' -> king;
            case 'p' -> pawn;
            default -> null;
        };
    }

    private Piece getWhitePiece(char piece) {
        return switch (piece) {
            case 'R' -> rook;
            case 'N' -> knight;
            case 'B' -> bishop;
            case 'Q' -> queen;
            case 'K' -> king;
            case 'P' -> pawn;
            default -> null;
        };
    }

    public Piece getPiece(Board board, char piece) {
        return board.isWhiteMove() ? getWhitePiece(piece) : getBlackPiece(piece);
    }
    public Piece getAdversarialPiece(Board board, char piece) {
        return board.isWhiteMove() ? getBlackPiece(piece) : getWhitePiece(piece);
    }

}
