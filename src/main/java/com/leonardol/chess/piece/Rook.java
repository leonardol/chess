package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class Rook extends GenericPiece {

    @NonNull
    private Board board;

    @Override
    public boolean canMove(Position from, Position to, Board board) {
        return !board.isOutOfRangeValue(from, to) && isMovingNonDiagonal(from, to) && isFreePath(from, to, board);
    }

    public boolean isCorrectMove(Position from, Position to) {
        return isMovingNonDiagonal(from,to);
    }

    public boolean isFreePath(Position from, Position to, Board board) {
        return isFreeNonDiagonalPath(from, to, board);
    }

}
