package com.leonardol.chess.piece;

import com.leonardol.chess.bean.DeltaMove;
import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class Pawn extends GenericPiece {

    @NonNull
    private Board board;

    @Override
    public boolean canMove(Position from, Position to, Board board) {
        return !board.isOutOfRangeValue(from, to) && isCorrectMove(from, to, board);
    }

    public boolean isCorrectMove(Position from, Position to, Board board) {
        return isMovingDiagonal(from, to, board) || isMovingVertical(from, to, board);
    }

    public boolean isMovingDiagonal(Position from, Position to, Board board) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        boolean isWhiteDiagonalMove = board.isWhiteMove() && Math.abs(deltaMove.getFile()) == 1 && deltaMove.getRank() == -1;
        boolean isBlackDiagonalMove = !this.board.isWhiteMove() && Math.abs(deltaMove.getFile()) == 1 && deltaMove.getRank() == 1;

        return board.getSquares()[to.getFile() + to.getRank() * Board.WIDTH] != 0
                && (isWhiteDiagonalMove || isBlackDiagonalMove);
    }

    public boolean isMovingVertical(Position from, Position to, Board board) {

        final int WHITE_STARTING_RANK = 6;
        final int BLACK_STARTING_RANK = 1;
        final int FIRST_DELTA_MOVE = 2;
        final int ORDINARY_DELTA_MOVE = 1;
        final int WHITE_THIRD_RANK = 5;
        final int BLACK_THIRD_RANK = 2;

        DeltaMove deltaMove = calculateDeltaMove(from, to);
        boolean isWhiteFirstMove = from.getRank() == WHITE_STARTING_RANK && deltaMove.getRank() == - FIRST_DELTA_MOVE;
        boolean isWhiteGenericMove = from.getRank() != WHITE_STARTING_RANK && deltaMove.getRank() == - ORDINARY_DELTA_MOVE;
        boolean isBlackFirstMove = from.getRank() == BLACK_STARTING_RANK && deltaMove.getRank() == FIRST_DELTA_MOVE;
        boolean isBlackGenericMove = from.getRank() != BLACK_STARTING_RANK && deltaMove.getRank() == ORDINARY_DELTA_MOVE;

        boolean isWhiteVerticalMove = board.isWhiteMove() && deltaMove.getFile() == 0
                && (isWhiteFirstMove && board.squareCharacter(new Position(to.getFile(), WHITE_THIRD_RANK)) == 0
                || isWhiteGenericMove);

        boolean isBlackVerticalMove = !board.isWhiteMove() && deltaMove.getFile() == 0
                && (isBlackFirstMove && board.squareCharacter(new Position(to.getFile(), BLACK_THIRD_RANK)) == 0
                || isBlackGenericMove);

        return board.squareCharacter(new Position(to.getFile(), to.getRank())) == 0
                && (isWhiteVerticalMove || isBlackVerticalMove);
    }

}
