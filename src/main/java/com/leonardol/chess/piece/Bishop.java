package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.springframework.stereotype.Component;

@Component
public class Bishop extends GenericPiece {

    @Override
    public boolean canMove(Position from, Position to, Board board) {
        return !board.isOutOfRangeValue(from, to) && isCorrectMove(from, to) && isFreePath(from, to, board);
    }

    public boolean isCorrectMove(Position from, Position to) {
        return isMovingDiagonal(from, to);
    }

    public boolean isFreePath(Position from, Position to, Board board) {
        return isFreeDiagonalPath(from, to, board);
    }
}
