package com.leonardol.chess.piece;

import com.leonardol.chess.bean.DeltaMove;
import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import com.leonardol.chess.exception.MoveException;

public abstract class GenericPiece implements Piece {

    @Override
    public void move(Position from, Position to, Board board) {
        if (this.canMove(from, to, board)) {
            board.getSquares()[to.getFile() + to.getRank() * Board.WIDTH] = board.squareCharacter(from);
            board.getSquares()[from.getFile() + from.getRank() * Board.WIDTH] = 0;
        } else throw new MoveException("Move not valid: " + from.toString() + " " + to.toString());
    }

    protected DeltaMove calculateDeltaMove(Position from, Position to) {
        return new DeltaMove(to.getFile() - from.getFile(),  to.getRank() - from.getRank());
    }

    protected boolean isMovingDiagonal(Position from, Position to) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        return deltaMove.getFile() != 0 && Math.abs((float) deltaMove.getRank() / (float) deltaMove.getFile()) == 1.0;
    }

    protected boolean isMovingNonDiagonal(Position from, Position to) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        return Math.abs(deltaMove.getFile()) != 0 && deltaMove.getRank() == 0
                || Math.abs(deltaMove.getRank()) != 0 && deltaMove.getFile() == 0;
    }

    protected boolean isFreeDiagonalPath(Position from, Position to, Board board) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        for (int i = 1, j = 1; i < Math.abs(deltaMove.getRank()) && j < Math.abs(deltaMove.getFile()); i++, j++) {
            int deltaRank = Integer.signum(deltaMove.getRank()) * i;
            int deltaFile = Integer.signum(deltaMove.getFile()) * j;
            Position p = new Position(from.getFile() + deltaFile, from.getRank() + deltaRank);
            if (board.squareCharacter(p) != 0) {
                return false;
            }
        }
        return true;
    }

    protected boolean isFreeHorizontalPath(Position from, Position to, Board board) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        if (deltaMove.getRank() == 0) {
            for (int i = 1; i < Math.abs(deltaMove.getFile()); i++) {
                int deltaFile = Integer.signum(deltaMove.getFile()) * i;
                Position p = new Position(from.getFile() + deltaFile, from.getRank());
                if (board.squareCharacter(p) != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean isFreeVerticalPath(Position from, Position to, Board board) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        if (deltaMove.getFile() == 0) {
            for (int i = 1; i < Math.abs(deltaMove.getRank()); i++) {
                int deltaRank = Integer.signum(deltaMove.getRank()) * i;
                Position p = new Position(from.getFile(), from.getRank() + deltaRank);
                if (board.squareCharacter(p) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean isFreeNonDiagonalPath(Position from, Position to, Board board) {
        DeltaMove deltaMove = calculateDeltaMove(from, to);
        return (deltaMove.getRank() == 0 && isFreeHorizontalPath(from, to, board)
                || deltaMove.getFile() == 0 && isFreeVerticalPath(from, to, board));
    }

}
