package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;

public interface Piece {

    boolean canMove(Position from, Position to, Board board);

    void move(Position from, Position to, Board board);

}
