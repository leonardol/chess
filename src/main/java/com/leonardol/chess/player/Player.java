package com.leonardol.chess.player;

import com.leonardol.chess.bean.Position;

public interface Player {

    void move(Position from, Position to);

}
