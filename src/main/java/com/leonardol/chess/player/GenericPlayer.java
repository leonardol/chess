package com.leonardol.chess.player;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import com.leonardol.chess.exception.MoveException;
import com.leonardol.chess.piece.Piece;
import com.leonardol.chess.piece.PieceFactory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Slf4j
@Component
public class GenericPlayer implements Player {

    private Board board;
    private PieceFactory pieceFactory;

    @Override
    public void move(Position from, Position to) {
        Board boardCopy = board.copy(board);
        Piece piece = squareToPiece(from, boardCopy);

        if (checkPosition(to, piece, boardCopy)) {

            piece.move(from, to, boardCopy);
            verifyOccurInCheckAndRemoveCheck(from, to, boardCopy);
            if (verifyInCheck(boardCopy)) boardCopy.addCheck();
            board.update(boardCopy);
            board.changeTurn();

            log.info(board.toString());

        } else throw new MoveException("Move not valid: " + from + " " + to);
    }

    public boolean checkPosition(Position to, Piece piece, Board board) {
        char square = board.squareCharacter(to);
        return piece != null && (square == 0 || pieceFactory.getAdversarialPiece(board, square) != null);
    }

    public boolean verifyInCheck(Board board) {
        int kingPosition = board.kingPosition();
        Position to = new Position(kingPosition % Board.WIDTH, kingPosition / Board.WIDTH);
        for (int j = 0; j < 8; j++) {
            for (int i = 0; i < 8; i++) {
                Position from = new Position(i, j);
                Piece piece = squareToPiece(from, board);
                if (piece != null && piece.canMove(from, to, board)) return true;
            }
        }
        return false;
    }

    private void verifyOccurInCheckAndRemoveCheck(Position from, Position to, Board board) {
        try {
            board.changeTurn();
            if (verifyInCheck(board)) throw new MoveException();
            board.changeTurn();
            board.removeCheck();
        } catch (MoveException e) {
            throw new MoveException("Move not valid: " + from + " " + to);
        }
    }

    private Piece squareToPiece(Position from, Board board) {
        char square = board.squareCharacter(from);
        return pieceFactory.getPiece(board, square);
    }

}
