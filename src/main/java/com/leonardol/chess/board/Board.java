package com.leonardol.chess.board;

import com.leonardol.chess.bean.Position;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Board {

    public static final int WIDTH = 8;
    public static final int HEIGHT = 8;
    private boolean isWhiteMove;
    private boolean whiteInCheck;
    private boolean blackInCheck;
    private char[] squares;

    public Board() {
        this.isWhiteMove = true;
        this.whiteInCheck = false;
        this.blackInCheck = false;
        this.squares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
    }

    public Board copy(Board actualBoard) {
        Board boardCopy = new Board();
        boardCopy.setWhiteMove(actualBoard.isWhiteMove());
        boardCopy.setWhiteInCheck(actualBoard.isWhiteInCheck());
        boardCopy.setBlackInCheck(actualBoard.isBlackInCheck());
        char[] destArray = new char[WIDTH * HEIGHT];
        System.arraycopy(squares, 0, destArray, 0, squares.length);
        boardCopy.setSquares(destArray);

        return boardCopy;
    }

    public void update(Board boardCopy) {
        setSquares(boardCopy.getSquares());
        setWhiteInCheck(boardCopy.isWhiteInCheck());
        setBlackInCheck(boardCopy.isBlackInCheck());
    }

    public boolean isOutOfRangeValue(Position from, Position to) {
        return isOutOfRange(from.getFile())
        || isOutOfRange(from.getRank())
        || isOutOfRange(to.getFile())
        || isOutOfRange(to.getRank());
    }

    private boolean isOutOfRange(int value) {
        return value < 0 || value > WIDTH - 1;
    }

    public void changeTurn() {
        isWhiteMove = !isWhiteMove;
    }

    public void addCheck() {
        if (isWhiteMove()) setBlackInCheck(true);
        else setWhiteInCheck(true);
    }

    public void removeCheck() {
        if (isWhiteMove()) setWhiteInCheck(false);
        else setBlackInCheck(false);
    }

    public int kingPosition() {
        return String.valueOf(getSquares()).indexOf(isWhiteMove() ? 'k' : 'K');
    }

    public char squareCharacter(Position position) {
        return squares[position.getFile() + position.getRank() * Board.WIDTH];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n");
        for (int i = 0; i < squares.length; i++) {
            if (squares[i] != 0) sb.append(squares[i]);

            else if ((i % 2 == 0 && (i / 8) % 2 == 0)
                    || (i % 2 != 0 && (i / 8) % 2 != 0)) sb.append(" ");
            else sb.append("#");

            if ((i + 1) % 8 == 0) sb.append("\n");
        }
        if (isWhiteInCheck()) sb.append("White in check\n");
        if (isBlackInCheck()) sb.append("Black in check\n");
        sb.append("Next move: ").append(isWhiteMove ? "White\n" : "Black\n");
        return sb.toString();
    }

}
