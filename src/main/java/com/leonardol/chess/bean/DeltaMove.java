package com.leonardol.chess.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeltaMove {

    private int file;
    private int rank;

}
