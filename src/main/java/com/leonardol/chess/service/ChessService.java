package com.leonardol.chess.service;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.player.GenericPlayer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChessService {

    @NonNull
    private GenericPlayer genericPlayer;

    public void play(Position from, Position to) {
        genericPlayer.move(from, to);
    }

}
