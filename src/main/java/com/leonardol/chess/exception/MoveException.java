package com.leonardol.chess.exception;

public class MoveException extends RuntimeException{

    public MoveException() {
        super();
    }

    public MoveException(String message) {
        super(message);
    }

}
