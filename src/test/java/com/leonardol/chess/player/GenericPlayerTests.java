package com.leonardol.chess.player;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import com.leonardol.chess.piece.King;
import com.leonardol.chess.piece.PieceFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class GenericPlayerTests {

    @Autowired
    private Board board;

    @Autowired
    private PieceFactory pieceFactory;

    @Autowired
    private GenericPlayer genericPlayer;

    @Autowired
    private King king;

    @Test
    void shouldSucceedOnCheckPositions() {
        char[] squares = new char[]{0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'k', 0, 0, 'q', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 'N', 0, 0, 0,
                0, 0, 0, 0, 'K', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0};
        board.setSquares(squares);
        Position from = new Position(4, 6);
        Position to = new Position(4, 7);

        boolean correctPositions = genericPlayer.checkPosition(to, king, board);

        assertThat(correctPositions).isTrue();

    }

    @Test
    void shouldSucceedOnVerifyCheck() {
        char[] squares = new char[]{0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'k', 0, 0, 'q', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 'K', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0};
        board.setSquares(squares);
        board.setWhiteMove(false);

        boolean isInCheck = genericPlayer.verifyInCheck(board);

        assertThat(isInCheck).isTrue();

    }

    @Test
    void shouldFailOnCheckPositions() {
        char[] squares = new char[]{0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'k', 0, 0, 'q', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 'N', 0, 0, 0,
                0, 0, 0, 0, 'K', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0};
        board.setSquares(squares);
        Position from = new Position(4, 6);
        Position to = new Position(4, 5);

        boolean correctPositions = genericPlayer.checkPosition(to, king, board);

        assertThat(correctPositions).isFalse();

    }

    @Test
    void shouldFailOnVerifyCheck() {
        char[] squares = new char[]{0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'k', 0, 0, 'q', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 'K', 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0};
        board.setSquares(squares);

        boolean isInCheck = genericPlayer.verifyInCheck(board);

        assertThat(isInCheck).isFalse();

    }

    @AfterEach
    void clean() {
        board.setWhiteMove(true);
        char[] squares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
        board.setSquares(squares);

    }
}
