package com.leonardol.chess.service;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import com.leonardol.chess.exception.MoveException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

@SpringBootTest
class ChessServiceTests {

    @Autowired
    private ChessService chessService;

    @Autowired
    private Board board;

    @BeforeEach
    void setUp() {
        Position from = new Position(4, 6);
        Position to = new Position(4, 4);
        chessService.play(from, to);
        from = new Position(3, 1);
        to = new Position(3, 3);
        chessService.play(from, to);
        from = new Position(4, 4);
        to = new Position(3, 3);
        chessService.play(from, to);
        from = new Position(4, 1);
        to = new Position(4, 3);
        chessService.play(from, to);
        from = new Position(3, 3);
        to = new Position(3, 2);
        chessService.play(from, to);
        from = new Position(4, 3);
        to = new Position(4, 4);
        chessService.play(from, to);
        from = new Position(3, 2);
        to = new Position(3, 1);
        chessService.play(from, to);
    }


    @Test
    void shouldPlay() {

        Position from = new Position(3, 0);
        Position to = new Position(3, 1);
        chessService.play(from, to);

        String squares = board.toString();

        assertThat(squares).isEqualTo("""

                rnb#kbnr
                pppq#ppp
                 # # # #
                # # # #\s
                 # #p# #
                # # # #\s
                PPPP PPP
                RNBQKBNR
                Next move: White
                """);
    }

    @Test
    void shouldSetInCheck() {

        String squares = board.toString();

        assertThat(squares).isEqualTo("""

                rnbqkbnr
                pppP#ppp
                 # # # #
                # # # #\s
                 # #p# #
                # # # #\s
                PPPP PPP
                RNBQKBNR
                Black in check
                Next move: Black
                """);
    }

    @Test
    void shouldFail() {
        final Position from = new Position(4, 4);
        final Position to = new Position(4, 5);

        assertThatExceptionOfType(MoveException.class).isThrownBy(() -> chessService.play(from, to))
                .withMessage("Move not valid: Position(file=4, rank=4) Position(file=4, rank=5)");

    }

    @AfterEach
    void clean() {
        board.setWhiteMove(true);
        char[] squares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
        board.setSquares(squares);

    }
}
