package com.leonardol.chess.board;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.piece.PieceFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class BoardTests {

    @Autowired
    private Board board;

    @Autowired
    private PieceFactory pieceFactory;

    @Test
    void shouldBeOutOfRange() {
        Position from = new Position(1, 2);
        Position to = new Position(4, 8);

        boolean isOutOfRange = board.isOutOfRangeValue(from, to);

        assertThat(isOutOfRange).isTrue();
    }

    @Test
    void shouldCopySquares() {
        Board boardCopy = board.copy(board);
        char[] squaresCopy = boardCopy.getSquares();

        boolean isCopy = Arrays.equals(squaresCopy, board.getSquares());

        assertThat(isCopy).isTrue();

    }

    @Test
    void shouldPrintSquares() {

        String squares = board.toString();

        assertThat(squares).isEqualTo("""

                rnbqkbnr
                pppppppp
                 # # # #
                # # # #\s
                 # # # #
                # # # #\s
                PPPPPPPP
                RNBQKBNR
                Next move: White
                """);
    }

    @Test
    void shouldNotBeOutOfRange() {
        Position from = new Position(1, 2);
        Position to = new Position(4, 5);
        boolean isOutOfRange = board.isOutOfRangeValue(from, to);

        assertThat(isOutOfRange).isFalse();
    }

    @Test
    void shouldFailCopySquares() {
        Board boardCopy = board.copy(board);
        char[] squaresCopy = boardCopy.getSquares();
        squaresCopy[25] ='g';

        boolean isCopy = Arrays.equals(squaresCopy, board.getSquares());

        assertThat(isCopy).isFalse();

    }

    @AfterEach
    void clean() {
        board.setWhiteMove(true);
        board.setWhiteInCheck(false);
        board.setBlackInCheck(false);
        char[] squares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
        board.setSquares(squares);

    }

}
