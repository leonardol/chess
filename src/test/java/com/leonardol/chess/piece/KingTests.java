package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class KingTests {

    @Autowired
    private King king;

    @Test
    void shouldSucceedOnCorrectMove() {
        Position from = new Position(1, 2);
        Position to = new Position(2, 2);

        boolean canMove = king.isCorrectMove(from, to);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldFailOnIncorrectMove() {

        Position from = new Position(1, 5);
        Position to = new Position(5, 8);

        boolean canMove = king.isCorrectMove(from, to);

        assertThat(canMove).isFalse();
    }

}
