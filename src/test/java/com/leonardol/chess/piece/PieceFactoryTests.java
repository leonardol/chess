package com.leonardol.chess.piece;

import com.leonardol.chess.board.Board;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class PieceFactoryTests {

    @Autowired
    private Board board;

    @Autowired
    private PieceFactory pieceFactory;

    @Test
    void shouldSucceedOnGettingAQueen() {
        Piece piece = pieceFactory.getPiece(board, 'Q');

        boolean isQueen = piece instanceof Queen;

        assertThat(isQueen).isTrue();

    }

    @Test
    void shouldSucceedOnGettingAnAdversarialQueen() {
        Piece piece = pieceFactory.getAdversarialPiece(board, 'q');

        boolean isQueen = piece instanceof Queen;

        assertThat(isQueen).isTrue();

    }

    @Test
    void shouldFailOnGettingAQueen() {
        Piece piece = pieceFactory.getPiece(board, 'q');

        boolean isQueen = piece instanceof Queen;

        assertThat(isQueen).isFalse();

    }

    @Test
    void shouldFailOnGettingAnAdversarialQueen() {
        Piece piece = pieceFactory.getAdversarialPiece(board, 'Q');

        boolean isQueen = piece instanceof Queen;

        assertThat(isQueen).isFalse();

    }

}
