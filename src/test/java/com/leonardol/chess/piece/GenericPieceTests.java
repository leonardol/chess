package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import com.leonardol.chess.exception.MoveException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

@SpringBootTest
class GenericPieceTests {

    @Autowired
    private Board board;

    @Autowired
    private Pawn pawn;

    @Test
    void shouldMovePiece() {
        char[] newSquares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                0, 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                'p', 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
        Position from = new Position(0, 1);
        Position to = new Position(0, 3);
        char[] squares = board.getSquares();
        board.setWhiteMove(false);

        pawn.move(from, to, board);
        boolean isCopy = Arrays.equals(newSquares, board.getSquares());

        assertThat(isCopy).isTrue();
    }

    @Test
    void shouldNotMovePiece() {
        Position from = new Position(0, 1);
        Position to = new Position(0, 3);
        char[] squares = board.getSquares();
        squares[16] ='Q';
        board.setWhiteMove(false);

        assertThatExceptionOfType(MoveException.class).isThrownBy(() -> pawn.move(from, to, board))
                .withMessage("Move not valid: Position(file=0, rank=1) Position(file=0, rank=3)");

    }

    @AfterEach
    void clean() {
        board.setWhiteMove(true);
        char[] squares = new char[]{'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r',
                'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P',
                'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'};
        board.setSquares(squares);

    }
}
