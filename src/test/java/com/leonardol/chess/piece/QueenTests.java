package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
class QueenTests {

    @Autowired
    private Queen queen;
    @MockBean
    private Board board;

    @Test
    void shouldSucceedOnMovingVertical() {
        Position from = new Position(1, 6);
        Position to = new Position(5, 6);

        boolean canMove = queen.isMovingNonDiagonal(from, to);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldSucceedOnMovingDiagonal() {
        Position from = new Position(1, 6);
        Position to = new Position(2, 5);

        boolean canMove = queen.isMovingDiagonal(from, to);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldSucceedOnFreePath() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'B', 'p', 0, 0, 0, 0, 0,
                0, 'p', 0, 'p', 0, 0, 0, 0,
                0, 'P', 'P', 'Q', 'P', 0, 0, 0,
                0, 0, 0, 0, 'p', 'b', 'p', 0,
                0, 0, 0, 0, 0, 0, 'l', 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(6, 0);
        Position to = new Position(6, 3);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(6,1))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(6,2))).thenReturn((char) 0);

        boolean canMove = queen.isFreePath(from, to, board);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldFailOnOnMovingNonVertical() {
        Position from = new Position(6, 0);
        Position to = new Position(5, 7);

        boolean canMove = queen.isMovingNonDiagonal(from, to);

        assertThat(canMove).isFalse();
    }

    @Test
    void shouldFailOnOnMovingNonDiagonal() {
        Position from = new Position(6, 0);
        Position to = new Position(5, 7);

        boolean canMove = queen.isMovingDiagonal(from, to);

        assertThat(canMove).isFalse();
    }

    @Test
    void shouldFailOnNotFreePath() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'B', 'p', 0, 0, 0, 0, 0,
                0, 'p', 0, 'p', 0, 0, 0, 0,
                0, 'P', 'P', 'Q', 'P', 0, 0, 0,
                0, 0, 0, 0, 'p', 'b', 'p', 0,
                0, 0, 0, 0, 0, 0, 'l', 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(6, 0);
        Position to = new Position(6, 7);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(6,1))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(6,2))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(6,3))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(6,4))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(6,5))).thenReturn('p');
        when(board.squareCharacter(new Position(6,6))).thenReturn('l');

        boolean canMove = queen.isFreePath(from, to, board);

        assertThat(canMove).isFalse();
    }

}
