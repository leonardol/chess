package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
class BishopTests {

    @Autowired
    private Bishop bishop;

    @MockBean
    private Board board;

    @Test
    void shouldSucceedOnCorrectMove() {
        Position from = new Position(1, 2);
        Position to = new Position(4, 5);

        boolean canMove = bishop.isCorrectMove(from, to);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldSucceedOnFreePath() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'B', 'p', 0, 0, 0, 0, 0,
                0, 'p', 0, 'p', 0, 0, 0, 0,
                0, 'P', 'P', 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 'p', 'b', 'p', 0,
                0, 0, 0, 0, 0, 0, 'l', 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(1, 2);
        Position to = new Position(4, 5);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(2,3))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(3,4))).thenReturn((char) 0);

        boolean canMove = bishop.isFreePath(from, to, board);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldFailOnIncorrectMove() {
        Position from = new Position(1, 5);
        Position to = new Position(5, 5);

        boolean canMove = bishop.isCorrectMove(from, to);

        assertThat(canMove).isFalse();
    }

    @Test
    void shouldFailOnNotFreePath() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 'B', 'p', 0, 0, 0, 0, 0,
                0, 'p', 0, 'p', 0, 0, 0, 0,
                0, 'P', 'P', 'Q', 'P', 0, 0, 0,
                0, 0, 0, 0, 'p', 'b', 'p', 0,
                0, 0, 0, 0, 0, 0, 'l', 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(1, 2);
        Position to = new Position(4, 5);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(2,3))).thenReturn((char) 0);
        when(board.squareCharacter(new Position(3,4))).thenReturn('Q');

        boolean canMove = bishop.isFreePath(from, to, board);

        assertThat(canMove).isFalse();
    }

}
