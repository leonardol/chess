package com.leonardol.chess.piece;

import com.leonardol.chess.bean.Position;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class KnightTests {

    @Autowired
    private Knight knight;


    @Test
    void shouldSucceedOnCorrectMove() {
        Position from = new Position(1, 2);
        Position to = new Position(0, 4);

        boolean canMove = knight.isCorrectMove(from, to);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldFailOnIncorrectMove() {
        Position from = new Position(1, 2);
        Position to = new Position(3, 4);

        boolean canMove = knight.isCorrectMove(from, to);

        assertThat(canMove).isFalse();
    }

}
