package com.leonardol.chess.piece;


import com.leonardol.chess.bean.Position;
import com.leonardol.chess.board.Board;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
class PawnTests {

    @Autowired
    private Pawn pawn;
    @MockBean
    private Board board;

    @Test
    void shouldSucceedOnDiagonalMove() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 'P', 'P', 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 'P', 'P', 0, 0, 0,
                0, 0, 0, 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(3, 4);
        Position to = new Position(4, 3);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);

        boolean canMove = pawn.isMovingDiagonal(from, to, board);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldSucceedOnVerticalMove() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 'P', 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 'P', 'P', 0, 0, 0,
                0, 0, 0, 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(true);
        Position from = new Position(3, 4);
        Position to = new Position(3, 3);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(4,3))).thenReturn((char)0);

        boolean canMove = pawn.isMovingVertical(from, to, board);

        assertThat(canMove).isTrue();
    }

    @Test
    void shouldFailOnDiagonalMove() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, '0', 0, 0, 0, 0,
                0, 0, 0, 0, '0', 0, 0, 0,
                0, 0, 'P', 'P', 'P', 0, 0, 0,
                0, 0, '0', '0', 0, 0, 0, 0,
                0, 0, 0, 'P', 'P', 0, 0, 0,
                0, 0, 0, 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(false);
        Position from = new Position(3, 3);
        Position to = new Position(4, 4);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(4,4))).thenReturn((char)0);


        boolean canMove = pawn.isMovingDiagonal(from, to, board);

        assertThat(canMove).isFalse();
    }

    @Test
    void shouldFailOnVerticalMove() {
        when(board.getSquares()).thenReturn(new char[]{
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, '0', 0, 0, 0, 0,
                0, 0, 0, 0, '0', 0, 0, 0,
                0, 0, 'P', 'P', 'P', 0, 0, 0,
                0, 0, '0', '0', 0, 0, 0, 0,
                0, 0, 0, 'P', 'P', 0, 0, 0,
                0, 0, 0, 0, 'P', 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
        });
        when(board.isWhiteMove()).thenReturn(false);
        Position from = new Position(3, 3);
        Position to = new Position(4, 4);
        when(board.isOutOfRangeValue(from, to)).thenReturn(false);
        when(board.squareCharacter(new Position(4,4))).thenReturn('0');


        boolean canMove = pawn.isMovingVertical(from, to, board);

        assertThat(canMove).isFalse();
    }
}
